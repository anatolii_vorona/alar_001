#!/usr/bin/python
# -*- coding: utf-8 -*-
from marshmallow import Schema


class UserSchema(Schema):
    class Meta:
        fields = ('user_id', 'user_name', 'user_pass', 'user_role')
