#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import jsonify
from flask import redirect
from flask import render_template
from flask import send_from_directory
from flask import session
from flask import url_for

from www import app, db
from .forms import LoginForm, UserForm
from .models import Users
from .schemas import UserSchema

STATIC_FOLDER = app.config['STATIC_FOLDER']
permissions_array = ['read', 'edit', 'add', 'delete']


def UserCan(task):
    # user_role = 1  = 0b1     => res = {'read':0,'edit':0,'add':0, 'delete':1}
    # user_role = 12 = 0b1100  => res = {'read':1,'edit':1,'add':0, 'delete':0}
    # user_role = 8  = 0b1000  => res = {'read':1,'edit':1,'add':0, 'delete':0}
    # user_role = 25 = 0b11001 => res = {'read':1,'edit':0,'add':0, 'delete':1}
    res = {'read': 0, 'edit': 0, 'add': 0, 'delete': 0}
    if 'id' in session and (task in permissions_array or task == 'show_permissions'):
        user_id = session['id']
        decimal_mask = 2 ** len(permissions_array) - 1
        decimal_perm = Users.query.get(user_id).user_role
        x = decimal_perm & decimal_mask
        permissions_vals = [int(i) for i in str(bin(x))[2:]]
        while len(permissions_vals) < len(permissions_array):
            permissions_vals.insert(0, 0)
        for k, (bool, key) in enumerate(zip(permissions_vals, permissions_array)):
            res[key] = bool
        if task == 'show_permissions':
            return res
        else:
            return res[task]
    return False


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(STATIC_FOLDER, 'favicon.ico', mimetype='image/x-icon')


@app.route('/static/<path:filename>')
def return_static(filename):
    return send_from_directory(STATIC_FOLDER, filename)


@app.route('/workplace')
def workplace():
    if UserCan('read'):
        return render_template('workplace.html', items=UserSchema(many=True).dump(Users.query.all()).data,
                               user_id=session['id'], permissions=UserCan('show_permissions'))
    return redirect(url_for('index'))


@app.route('/json/users')
def json_get_users():
    if 'id' in session:
        return jsonify({'result': UserSchema(many=True).dump(Users.query.all()).data})
    return '{}', 403


@app.route('/')
@app.route('/login')
def index():
    return render_template("index.html", form=LoginForm(csrf_enabled=False))


@app.route('/logout')
def logout():
    session.pop('id', None)
    return redirect(url_for('index'))


@app.route('/login', methods=['POST'])
def login():
    form = LoginForm(csrf_enabled=False)
    if not form.validate():
        return jsonify({'result': form.errors})
    status = Users.query.get(1)
    if status is None:
        # nobody home
        sysdba = Users(
            user_id=1,
            user_name=form.name.data,
            user_pass=form.password.data,
            user_role=15
        )
        db.session.add(sysdba)
        db.session.commit()
        session['id'] = 1
        return redirect(url_for('workplace'))
    else:
        # access checking
        passwords = [x.user_pass for x in Users.query.filter_by(user_name=form.name.data)]
        if len(passwords) == 1 and form.password.data in passwords:
            # accepted
            session['id'] = Users.query.filter_by(user_name=form.name.data)[0].user_id
            return redirect(url_for('workplace'))
        else:
            # failed
            return render_template("index.html", form=LoginForm(csrf_enabled=False))


@app.route('/add_user', methods=['GET'])
def add_user_form():
    form = UserForm(csrf_enabled=False)
    if UserCan('add'):
        return render_template('users.html', form=form, headline='Add:')
    else:
        return redirect(url_for('index'))


@app.route('/add_user', methods=['POST'])
def add_user():
    form = UserForm(csrf_enabled=False)
    if not form.validate():
        return jsonify({'result': form.errors})
    if UserCan('add'):
        # adding new user
        user = Users(
            user_name=form.name.data,
            user_pass=form.password.data,
            user_role=form.perms.data
        )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('workplace'))
    else:
        return redirect(url_for('index'))


@app.route('/login_as/<int:user_id>')
def login_as(user_id):
    session['id'] = user_id
    return redirect(url_for('workplace'))


@app.route('/del_user/<int:user_id>')
def del_user(user_id):
    if session['id'] != user_id and UserCan('delete'):
        user = Users.query.get(user_id)
        db.session.delete(user)
        db.session.commit()
    return redirect(url_for('workplace'))


@app.route('/edit_user/<int:user_id>', methods=['GET'])
def edit_user_form(user_id):
    form = UserForm(csrf_enabled=False)
    if UserCan('edit'):
        return render_template('edit_users.html', form=form, headline='Edit:', user=Users.query.get(user_id))
    else:
        return redirect(url_for('index'))


@app.route('/edit_user/<int:user_id>', methods=['POST'])
def edit_user(user_id):
    form = UserForm(csrf_enabled=False)
    if not form.validate():
        return jsonify({'result': form.errors})
    if UserCan('edit'):
        user = Users.query.get(user_id)
        user.user_name = form.name.data
        user.user_pass = form.password.data
        user.user_role = form.perms.data
        db.session.commit()
        return redirect(url_for('workplace'))
    else:
        return redirect(url_for('index'))
