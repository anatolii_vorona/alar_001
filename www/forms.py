#!/usr/bin/python
from flask_wtf import Form
from wtforms import StringField, SubmitField, IntegerField
from wtforms.validators import InputRequired, DataRequired


class LoginForm(Form):
    name = StringField('name', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    submit = SubmitField('Submit')


class UserForm(Form):
    name = StringField('name', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    perms = IntegerField('perms', validators=[InputRequired()])
    submit = SubmitField('Submit')
