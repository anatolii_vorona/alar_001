#!/usr/bin/python
# -*- coding: utf-8 -*-
from www import db


class Users(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(32), index=True, unique=True)
    user_pass = db.Column(db.String(32))
    user_role = db.Column(db.Integer)  # rights = ['read', 'edit', 'add', 'delete']

    def __repr__(self):
        return '<Users %r>' % self.user_name
